//
//  Article.swift
//  Basic_01
//
//  Created by eva on 2018/5/27.
//  Copyright © 2018年 Lextronic. All rights reserved.
//
import Foundation
import UIKit

class Article {  // 把 App 用到的資料包裝成查表(Model Class), No Acion, No NSObject 繼承
    
    let locationName: String?
    let parameterName: String?
    
    init(rawData: [String: Any]) { // initializer: like constructor 給初始值給先前宣告的數
        locationName = rawData["locationName"] as? String
        parameterName = rawData["parameterName"] as? String
    }
}
