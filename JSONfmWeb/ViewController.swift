//
//  ViewController.swift
//  JSONfmWeb
//
//  Created by eva on 2018/5/27.
//  Copyright © 2018年 Lextronic. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var table: UITableView!
    var articles = [Article]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Alamofire.request("http://data.taipei/opendata/datalist/apiAccess?scope=resourceAquire&rid=e6831708-02b4-4ef8-98fa-4b4ce53459d9").responseJSON(completionHandler: { response in
            if response.result.isSuccess {
                let json: JSON = try! JSON(data: response.data!)
                print("json")
                if let results = json["result"]["results"].arrayObject as? [[ String: Any]]{
                    // print(results)
                    var articles = [Article]()
                    for articleDict in results {
                        let article = Article(rawData: articleDict)
                        print(articleDict)
                        articles.append(article)
                        self.articles = articles
                    }
                } else {    print("error")   }
                print(self.articles)
                self.table.reloadData()
            }
        })
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let article = articles[indexPath.row]
        cell.textLabel?.text = article.locationName
        cell.detailTextLabel?.text = article.parameterName
        return cell
    }
}
